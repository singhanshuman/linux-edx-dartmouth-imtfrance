#include<stdio.h>

double temp(double,char);
double distance(double,char);
double weight(double, char);

int main(void){
    int conversions;
    double value;
    char check;
    int i=0;
    scanf("%d",&conversions);
    
    while(i<conversions){
        scanf("%lf %c",&value,&check);
        if (check=='c'){
            temp(value,check);
        }
        else if (check=='m'){
            distance(value,check);
        }
        else{
            weight(value,check);
        }
        i++;
    }
    
    
    return 0;
    
}

double temp(double value,char check){
    double fahr = ((1.8*value)+32);
    printf("%lf f\n",fahr);
    return 0;
    
}
double weight(double value,char check){
    double nvalue = 0.002205*value;
    printf("%lf lbs\n",nvalue);
    return 0;
    
}
double distance(double value,char check){
    double feet = 3.2808*value;
    printf("%lf ft\n",feet);
    return 0;
}
