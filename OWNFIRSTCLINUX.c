// FIRST C CODE FOR SYSTEM!
// function main takes either 0 or 2 arguments - first is int and second is a pointer of a pointer!


#include <stdlib.h>

#include <string.h>
/*Write your C code here*/
#include <stdio.h>

int main(int argc, char **rec){
	
	int num;
	double price,total;
	num = atoi(rec[1]);
	price = atof(rec[2]);
	total = num*price;
	
	if(argc==0){
		printf("Invalid input\n.");
	}else{
		printf("%d plants for %.2lf each cost %.2lf dollars.",num,price,total);
	}
	
	return 0;
}
