#include<stdio.h>
int memory(int);
int main(void){
    int inputs = 0;
    int result;
    scanf("%d",&inputs);
    result = memory(inputs);
    printf("%d bytes",result);
}
int memory(int iter){
    int i;
    int value;
    char type;
    int calc=0;
    int storage=0;
    for(i=0;i<iter;i++){
        scanf("%d %c",&value,&type);
        
        if(type=='i'){
            calc = value*sizeof(int);
        }
        else if(type=='c'){
            calc = value*sizeof(char);
        }
        else if (type=='d'){
            calc = value*sizeof(double);
        }else{
            printf("Invalid Tracking Code Type");
        }
        
        storage = storage+calc;
    }
    return storage;
}