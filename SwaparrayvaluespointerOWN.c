#include<stdio.h>
void reverseArray(int *);
int main(void){
    int number[7];
    int i;
    for(i=0;i<6;i++){
    scanf("%d",&number[i]); // adds values to the array
        
    }
    reverseArray(number); // call function reverseArray with number array as the argument
    int j;
    for(j=0;j<6;j++){
    printf("%d ",number[j]);
    }
    return 0;
}

void reverseArray(int * number){ // function starts. // the function takes - WHAT IS AT THE POINTED PLACE OF THE ARRAY NUMBER? Argument
    int temp0;///for storing value 
    int temp1;///for storing value 
    int temp2;///for storing value 
    temp0 = number[0];
    temp1  = number[1];
    temp2 = number[2];
   
   
    *number = *(number+5);       // the interpretation is - What is pointed at 0 of number array is to be replaced with what is pointed or stored at number array place 6, i.e. number[5]
    *(number+1) = *(number+4);
    *(number+2) = *(number+3);
   
    *(number+3) = temp2;
    
    *(number+4) = temp1;
    *(number+5) = temp0;
}