#include <stdio.h>

void change(int *);//Write your function prototype here

int main(void){
    //! showMemory (start=65520)
	int age;
	int *ageAddr = &age;
	scanf("%d", ageAddr);
	printf("Your current age is %d.\n", age);

	change(&age);//Write your function call here

	printf("Your new age will be %d!\n", age);
	return 0;
}


void change(int * age){  //Write your function here
    if(*age<21){
        *age=*age*2;
    }else{
        *age=*age-10;
    }
}
 