#include <stdio.h>     // finding how much behind the leaderboard in a game

void behind(int *, int);

int main(void) {
    int array[10];
    int N, i;
    
    scanf("%d", &N);
    for (i=0; i<N; i++) {
        scanf("%d", &array[i]);
    }
    behind(array, N);
    for (i=0; i<N; i++) {
        printf("%d\n", array[i]);
    }
    
    return 0;
}

/* Write your function behind() here: */
void behind(int *score, int players){
    int max=-1;
    int i;
    for(i=0;i<players;i++){
       if(score[i]>max){
            max = score[i];
        }
        
    }
    int j;
    for(j=0;j<players;j++){
        int lag = max-score[j];
        score[j] = lag; 
    }
    
    
    
}