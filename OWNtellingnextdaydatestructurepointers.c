#include <stdio.h>

struct date {
        int year;
        int month;
        int day;
    };

/* function prototypes */
void printDate(struct date);
void readDate(struct date *);
struct date advanceDay(struct date); 

int main(void) {
	struct date today, tomorrow;
	readDate(&today);
	printDate(today);
	tomorrow = advanceDay(today);
	printDate(tomorrow);
	return 0;
}

void readDate(struct date *receive){
    scanf("%d %02d %02d",&receive->year,&receive->month,&receive->day);
}

void printDate(struct date receive){
    printf("%02d/%02d/%d\n",receive.month,receive.day,receive.year);
}

struct date advanceDay(struct date receive){
    if(receive.month==1||receive.month==3||receive.month==5||receive.month==7||receive.month==8||receive.month==10){
        if(receive.day==31){
            receive.day=1;
            receive.month++;
        }
        else{
            receive.day++;
        }
    } else if(receive.month==4||receive.month==6||receive.month==9||receive.month==11){
            if(receive.day==30){
                receive.day=1;
                receive.month++;
            }
            else{
                  receive.day++;
            }
    }else if(receive.month==12){
                if(receive.day==31){
                receive.day=1;
                receive.month=1;
                receive.year++;
                }else{
                    receive.day++;
                }
            }
    else if (receive.month==2){
                if(receive.day==28){
                          receive.day=1; 
                          receive.month++;
                }else{
                    receive.day++;
                }
                
                
            }
    
    return(receive);
}