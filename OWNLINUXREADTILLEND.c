
// THIS FILE IS USED TO ITERATE TILL THE END OF THE GIVEN MARKS AND TELLS WHETHER The FIRST LISTED MARK IS GREATEST AMONG ALL OR NOT!


#include <stdio.h>
int main(void){
	FILE * reading;
	reading = fopen("gradeComparison.txt","r");
	double my;
	int i=1,position=0;
	int result=0,message;
	fscanf(reading,"%lf",&my);
	double other;
	int last=0;
	while(!last){
		message = fscanf(reading,"%lf",&other);
		if (message!=EOF){
			if(my>other){
				result=1;i++;
			}else{
				result=0;i++;
				position = i;
				last=1;
			}
		}else{
			last=1;
		}
	}
	if(result==1){
		printf("Yes");
	}else{
		printf("No %d",position);
	}
	fclose(reading);
	return 0;
}
